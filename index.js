import './system.js'

const notifyFallback = (message) => {
    console.info('Notification:', message)
}

export default (message) => {
    let notify = notifyFallback
    const notificationSystem = document.querySelector('notification-system')

    if (notificationSystem && typeof notificationSystem.notify === 'function') {
	notify = notificationSystem.notify
    }

    return notify(message)
}
